package com.example.mlcollector;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onCameraButtonClick(View view) {
        Intent intent = new Intent(this, CameraActivity.class);
        String message = "1";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
}